#!/bin/bash

# ---------------------------------------------
# clean up and destroy password on script exit
# ---------------------------------------------
function cleanup() {
  rm -rf authdir/ /root/.docker/config.json
  export PACKTIVITY_AUTH_LOCATION=""
  export RECAST_REGISTRY_HOST=""
  export RECAST_REGISTRY_USERNAME=""
  export RECAST_REGISTRY_PASSWORD=""
  export YADAGE_SCHEMA_LOAD_TOKEN=""
  export RECAST_AUTH_USERNAME=""
  export RECAST_AUTH_PASSWORD=""
  export YADAGE_INIT_TOKEN=""
  eval $(recast auth destroy)
}
trap cleanup EXIT
trap cleanup SIGINT
trap cleanup SIGQUIT
trap cleanup SIGTSTP
# ---------------------------------------------

# authentificate with credentials
if [ -z "$RECAST_USER" ]; then
  echo "Enter user name:"
  read RECAST_USER
fi
if [ -z "$RECAST_PASS" ]; then
  echo "Enter password:"
  read -s RECAST_PASS
fi
if [ -z "$RECAST_TOKEN" ]; then
  echo "Enter Gitlab API access token [default=RECAST_PASS]:"
  read RECAST_TOKEN
  RECAST_TOKEN=${RECAST_TOKEN:${RECAST_PASS}}
fi

eval "$(recast auth setup -a $RECAST_USER -a $RECAST_PASS -a $RECAST_TOKEN -a default)"
eval "$(recast auth write --basedir authdir)"

# add analysis temporarily to catalogue
$(recast catalogue add $PWD)
recast catalogue ls
recast catalogue describe examples/monoHworkflow
recast catalogue check examples/monoHworkflow

# run workflow
rm -rf recast-myrun/
recast run examples/monoHworkflow --tag myrun --backend docker
#zip -r recast-myrun.zip recast-myrun

