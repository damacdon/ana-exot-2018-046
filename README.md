# Workflow for MonoHbb analysis 2020

## Necessary steps before you run :

You will need docker ...
You will need cvmfs (I think ... it was necessary to run the docker image allone. )

Make sure if you run locally (i.e. in your laptop) that you have recast atlas istalled. This is super easy 

```
pip install recast-atlas
```


## Setup : 

```
eval "$(recast auth setup -a $RECAST_USER -a $RECAST_PASS -a $RECAST_TOKEN -a default)"
eval "$(recast auth write --basedir authdir)"

```
What should you use as :
  * RECAST_USER :: cern account user name **OR** service account user name 
  * RECAST_PASS :: cern account password **OR** service account password
  * RECAST_TOKEN :: cern account password **OR** personal project token. 
  Comment from Danika MacDonell : For now, it's also necessary to set  RECAST_TOKEN to your ATLAS password due to a bug in recast-atlas.


Now add the analysis to recast catalogue: 

```
$(recast catalogue add $PWD) 
```


To run :

```
recast run examples/monoHworkflow  --backend docker

```

or you can use --tag to specify which folder will be your output folder


```
recast run examples/monoHworkflow --tag myrun --backend docker
```


This will create a folder named recast-myrun which will contain:
 * seperate folders for each stage in your steps.yml and
 * a _yadage folder.

Inside the stages  folders you can find _pactivity folder which contains log files necessary for debugging  
If the folder recast-myrun exists and is empty all is cool. If it has has the formentioned folders you need to delete them before running. If you have any other file you don't have to delete those too.
In case you don't specify the tag each time you will get a new recast-<some weird name>

## Running using local data

Now for running with local data you will need to follow the instructions in **Recast for ATLAS** [documentation](https://recast-docs.web.cern.ch/recast-docs/workflowauthoring/running/#using-local-data)
**There is a caveat in this instruction**
To work the inputdata folder needs to be in the output directory that recast creats... The only way I could make it work was to put the input data directory inside the recast-myrun and only delete the _yadage and stages folders each time I want to rerun not the inputdata ...

